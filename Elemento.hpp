// Copyright 2015 Lucas Mauro de Souza

#ifndef ELEMENTO_HPP
#define ELEMENTO_HPP

/*!
 * Representa uma elemento de uma lista simples encadeada. Nesta estrutura, os
 * elementos são adicionados formando uma conexão entre o elemento atual e o próximo.
 */



template<typename T>
class Elemento {
 private:
  T *info;
  Elemento<T>* _next;

/*!
* Constrói um elemento guardando o ponteiro no variável info
* e apontando para um próximo elemento referenciado pelo ponteiro do next.
* 
* @param info Valor mantido pelo elemento
* @param _next Ponteiro para o próximo elemento de uma lista
*/
 public:
  Elemento(const T& info, Elemento<T>* next) : info(new T(info)), _next(next) {}

  ~Elemento() {
    delete info;
  }

    /*!
    * Retorna o ponteiro para o próximo elemento em relação a este mesmo elemento.
    */
  Elemento<T>* getProximo() const  {
    return _next;
  }

    /*!
    * Retorna o valor que o elemento está guardando/representando.
    */
  T getInfo() const {
    return *info;
  }

    /*!
    * Define o próximo elemento da lista em relação ao elemento atual.
    *
    * @param next Ponteiro para o próximo elemento em relação ao elemento atual.
    */
  void setProximo(Elemento<T>* next) {
    _next = next;
  }
};

#endif
