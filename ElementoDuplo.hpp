// Copyright 2015 Lucas Mauro de Souza

/*!
 * Representa um elemento de uma lista duplamente encadeada, estrutura.
 * onde os elementos são interligados formando uma conexão entre o elemento .
 * atual com o sucessor e antecessor.
 */
template <typename T>
class ElementoDuplo {
 private:
    ElementoDuplo<T> *antecessor;  //! Representa um ponteiro para o elemento.
    ElementoDuplo<T> *sucessor;  //! Representa um ponteiro para o elemento.
     T *info;  //! O valor apontado pelo elemento.

 public:
    /*!
     * Constrói um elemento duplo
     * 
     * @param: antecessor O elemento duplo antecessor que será apontado pelo atual.
     * @param: info O valor que será apontado pelo elemento duplo atual.
     * @param: sucessor O elemento duplo sucessor que será apontado pelo atual.
     */
    ElementoDuplo(ElementoDuplo<T> *antecessor, const T& info,
                                      ElementoDuplo<T> *sucessor) {
        this->antecessor = antecessor;
        this->info = new T(info);
        this->sucessor = sucessor;
    }

    /*!
     * Remove o valor armazenado no elemento duplo.
     */
    ~ElementoDuplo() {
        delete info;
    }

    /*!
     * Retorna o valor que está apontando para o antecessor.
     */
    ElementoDuplo<T> *getAntecessor() const {
        return this->antecessor;
    }

    /*!
     * Retorna o valor que está apontando para o secessor.
     */
    ElementoDuplo<T> *getSucessor() const {
        return this->sucessor;
    }

    /*!
     * Retorna o valor que está apontando para a informação armazenada no
     * elemento duplo.
     */
    T getInfo() const {
        return *info;
    }

    /*!
     * Define um sucessor em relação ao elemento atual.
     * 
     * @param: novo O novo elemento duplo que será apontado pelo atual.
     */
    void setSucessor(ElementoDuplo<T> *novo) {
        this->sucessor = novo;
    }

    /*!
     * Define um antecessor em relação ao elemento atual.
     * 
     * @param: novo O novo elemento duplo que será apontado pelo atual.
     */
    void setAntecessor(ElementoDuplo<T> *novo) {
        this->antecessor = novo;
    }
};
