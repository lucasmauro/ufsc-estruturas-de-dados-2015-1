// Copyright 2015 Lucas Mauro de Souza

#include "Elemento.hpp"
#include "ListaEnc.hpp"

/*
* Representa uma fila encadeada. Nesta estrutura os elementos são interligados virtualmente.
* Todo elemento todo elemento é adicionado exclusivamente pelo fim, e retirado exclusivamente pelo início.
*/
template <typename T>
class FilaEnc : private ListaEnc<T> {
 private:
    Elemento<T> *fim;  //! Ponteiro para o último elemento da lista.

 public:
    /*!
    * Construtor da fila.
    */
    FilaEnc<T>() {
    }

    /*!
    * Destrutor da fila.
    */
    ~FilaEnc() {
        ListaEnc<T>::destroiLista();
    }

    /*!
    * Inclui um elemento ao fim da fila.
    *
    * @param dado Dado a ser adicionado à fila.
    */
    void inclui(const T& dado) {
        if (ListaEnc<T>::listaVazia()) {
            ListaEnc<T>::adicionaNoInicio(dado);
            this->fim = ListaEnc<T>::head;
        } else {
            Elemento<T> *ultimo = new Elemento<T>(dado, NULL);
            this->fim->setProximo(ultimo);
            this->fim = ultimo;
            ListaEnc<T>::size++;
        }
    }

    /*!
    * Retira um elemento do fim da fila.
    */
    T retira() {
        T dado = ListaEnc<T>::retiraDoInicio();
        if (ListaEnc<T>::size == 0) {
            this->fim = NULL;
        }
        return dado;
    }

    /*!
    * Retorna uma cópia do último elemento da fila.
    */
    T ultimo() {
        if (ListaEnc<T>::listaVazia()) {
            throw("A fila está vazia");
        }
        return this->fim->getInfo();
    }

    /*
    * Retorna uma cópia do primeiro elemento da fila.
    */
    T primeiro() {
        if (ListaEnc<T>::listaVazia()) {
            throw("A fila está vazia");
        }
        return ListaEnc<T>::head->getInfo();
    }

    /*!
    * Retorna verdadero ou falso na verificação de se a fila está vazia.
    */
    bool filaVazia() {
        return ListaEnc<T>::listaVazia();
    }

    /*!
    * Limpa todos os valores contidos na fila.
    */
    void limparFila() {
        ListaEnc<T>::destroiLista();
    }
};

