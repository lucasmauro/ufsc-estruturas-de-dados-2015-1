// Copyright 2015 Lucas Mauro de Souza

#include "ElementoDuplo.hpp"

/*!
* Representa uma lista duplamente encadeada. Nesta estrutura, os elementos
* possuem ligação com os seus elementos antecessores e sucessores.
* Isto facilita a navegação na lista em ambas as direções.
*/

template <typename T>
class ListaDupla {
 private:
  ElementoDuplo<T> *head;  //! Ponteiro para o primeiro elemento da lista.
  int size;  //! Indica o tamaho da lista.

 public:
  /*!
  * Construtor da lista.
  */
  ListaDupla() {
    this->head = NULL;
    this->size = 0;
  }

  /*!
  * Destrutor da lista.
  */
  ~ListaDupla() {
    this->destroiListaDuplo();
  }

  /*!
  * Adiciona um elemento ao início da lista.
  *
  * @param dado Dado a ser inserido à lista como forma de elemento.
  */
  void adicionaNoInicioDuplo(const T& dado) {
    ElementoDuplo<T> *novo = new ElementoDuplo<T>(NULL, dado, this->head);
    this->head = novo;
    if (novo->getSucessor() != NULL) {
        novo->getSucessor()->setAntecessor(novo);
    }
    this->size++;
  }

  /*!
  * Retira um elemento do início da lista.
  */
  T retiraDoInicioDuplo() {
    if (this->listaVazia()) {
      throw("A lista está vazia; impossível retirar elementos");
    }

    T dado = this->head->getInfo();
    this->head = this->head->getSucessor();
    if (this->head != NULL) {
        this->head->setAntecessor(NULL);
        delete this->head->getAntecessor();
    }
    this->size--;
    return dado;
  }

  /*!
  * Elimina um elemento do início da lista.
  */
  void eliminaDoInicioDuplo() {
    if (this->listaVazia()) {
      throw("A lista está vazia; impossível retirar elementos");
    }

    this->head = this->head->getSucessor();
    if (this->head != NULL) {
      this->head->setAntecessor(NULL);
      delete this->head->getAntecessor();
    }

    this->size--;
  }

  /*!
  * Adiciona um elemento enviado pelo usuário à posição definida por ele.
  *
  * @param dado Dado a ser inserido à lista como forma de elemento.
  * @param pos Posição onde o dado será inserido.
  */
  void adicionaNaPosicaoDuplo(const T& dado, int pos) {
    if (pos == 0 || this->listaVazia()) {
        this->adicionaNoInicioDuplo(dado);
    } else if (pos >= this->size || pos < 0) {
        throw("Posicao inválida.");
    }
    ElementoDuplo<T> *novo;
    ElementoDuplo<T> *anterior = this->head;
    for (int contagem = 0; contagem < pos-1; contagem++) {
        if (anterior == NULL) {
            throw("Não é possível adicionar valores.");
        }
        anterior = anterior->getSucessor();
    }
    novo = new ElementoDuplo<T>(anterior, dado, anterior->getSucessor());
    if (novo->getSucessor() != NULL) {
        novo->getSucessor()->setAntecessor(novo);
    }
    anterior->setSucessor(novo);
    this->size++;
  }

  /*!
  * Retorna a posição na lista onde se encontra determinado elemento.
  *
  * @param dado Dado enviado pelo usuário para descobrimento da posição;
  */
  int posicaoDuplo(const T& dado) const {
    if (this->listaVazia()) {
        throw("A lista esta vazia.");
    }
    ElementoDuplo<T> *auxiliar = this->head;
    for (int contador = 0; contador <= this->size-1; contador++) {
        if (auxiliar->getInfo() == dado) {
            return contador;
        }
        auxiliar = auxiliar->getSucessor();
        if (auxiliar == NULL) {
            throw("Temporario foi detectado como nulo na posiçao "+contador);
        }
    }
    throw("O elemento não pertence à lista duplamente encadeada");
  }

  /*!
  * Retorna a posição de memória onde se encontra determinado elemento.
  *
  * @param dado Dado enviado pelo usuário para descobrimento da posição de memória;
  */
  T* posicaoMemDuplo(const T& dado) const {
    int posicao = this->posicaoDuplo(dado);
    return &posicao;
  }

  /*!
  * Retorna verdadeiro ou falso se a lista contém um determinado elemento.
  *
  * @param dado Dado a enviado pelo usuário para ser buscado na lista.
  */
  bool contemDuplo(const T& dado) {
    if (this->listaVazia()) {
      throw("A lista está vazia.");
    }
    ElementoDuplo<T> *elemento = this->head;
    for (int i = 0; i <= this->size-1; i++) {
      if (elemento->getInfo() == dado) {
        return true;
      }
    }
    return false;
  }

  /*!
  * Retira um elemento a partir da posição definida pelo usuário.
  *
  * @param pos Posição enviada pelo usuário para remoção de elemento.
  */
  T retiraDaPosicaoDuplo(int pos) {
    if (this->listaVazia()) {
        throw("A lista está vazia; impossível retirar elementos.");
    } else if (pos >= this->size || pos < 0) {
        throw("Posição inválida");
    } else if (pos == 0) {
        return this->retiraDoInicioDuplo();
    }

    T valor;
    ElementoDuplo<T> *auxiliar = this->head;
    for (int i = 0; i < pos-1; i++) {
        auxiliar = auxiliar->getSucessor();
        if (auxiliar == NULL) {
            throw("O valor foi detectado como nulo na posição "+i);
        }
    }
    ElementoDuplo<T> *remover = auxiliar->getSucessor();
    valor = remover->getInfo();
    auxiliar->setSucessor(remover->getSucessor());
    if (auxiliar->getSucessor() != NULL) {
        remover->getSucessor()->setAntecessor(auxiliar);
    }
    delete remover;
    this->size--;
    return valor;
  }

  /*!
  * Adiciona um elemento ao fim da lista.
  *
  * @param dado Dado a ser adicionado à lista em forma de elemento.
  */
  void adicionaDuplo(const T& dado) {
    if (this->listaVazia()) {
      return this->adicionaNoInicioDuplo(dado);
    }
    ElementoDuplo<T> *elemento = this->head;

    for (int i = 0; i < this->size-1; i++) {
      elemento = elemento->getSucessor();
      if (elemento == NULL) {
            throw("O valor foi detectado como nulo na posiçao: " +i);
      }
    }
    ElementoDuplo<T> *novo = new ElementoDuplo<T>(elemento, dado, NULL);
    elemento->setSucessor(novo);
    this->size++;
  }

  /*!
  * Retira o último elemento da lista.
  */
  T retiraDuplo() {
    if (this->listaVazia()) {
      throw("Impossível retirar elementos; a lista está vazia");
    }

    ElementoDuplo<T> *elemento = this->head;

    for (int i = 0; i < this->size-1; i++) {
      elemento = elemento->getSucessor();
    }

    T dado = elemento->getInfo();
    elemento->getAntecessor()->setSucessor(NULL);
    delete elemento;
    this->size--;

    return dado;
  }

  /*!
  * Retira um elemento específico da lista, definido pelo usuário.
  *
  * @param dado Dado especificado pelo usuário para remoção na lista.
  */
  T retiraEspecificoDuplo(const T& dado) {
    ElementoDuplo<T> *auxiliar = this->head;
    ElementoDuplo<T> *elemento;
    bool encontrado = false;
    for (int i = 1; i < this->size; i++) {
      if (auxiliar->getInfo() == dado) {
        elemento = auxiliar;
        encontrado = true;
      }
      auxiliar = auxiliar->getSucessor();
    }

    if (encontrado == false) {
      throw("Elemento não encontrado.");
    }

    T valor = elemento->getInfo();

    if (elemento->getSucessor() == NULL) {
      elemento->getAntecessor()->setSucessor(NULL);
    } else {
      ElementoDuplo<T> *antecessor = elemento->getAntecessor();
      ElementoDuplo<T> *sucessor = elemento->getSucessor();
      antecessor->setSucessor(sucessor);
      sucessor->setAntecessor(antecessor);
    }

    delete elemento;

    return valor;
  }

  /*!
  * Mostra o valor do elemento na posição definida pelo usuário.
  *
  * @param pos Posição definida pelo usuário.
  */
  T mostra(int pos) {
    ElementoDuplo<T> *elemento = this->head;
    for (int i = 0; i < this->size-1; i++) {
      elemento = elemento->getSucessor();
    }
    return elemento->getInfo();
  }

  /*!
  * Adiciona em ordem um elemento definido pelo usuário.
  *
  * @param data Dado enviado pelo usuário para ser adicionado como forma de elemento.
  */
  void adicionaEmOrdem(const T& data) {
    if (this->listaVazia()) {
      return this->adicionaNoInicioDuplo(data);
    }
    ElementoDuplo<T> *auxiliar = this->head;
    int posicao = 0;
    while (posicao < this->size-1 && this->maior(data, auxiliar->getInfo())) {
        auxiliar = auxiliar->getSucessor();
        posicao++;
    }
    if (this->maior(data, auxiliar->getInfo())) {
        this->adicionaNaPosicaoDuplo(data, posicao+1);
    } else {
        this->adicionaNaPosicaoDuplo(data, posicao);
    }
  }

  /*!
  * Retorna o tamanho da lista.
  */
  int verUltimo() {
    return this->size;
  }

  /*!
  * Verifica se a lista está vazia e retorna verdadeiro ou falso.
  */
  bool listaVazia() const {
    return this->size == 0;
  }

  /*!
  * Verifica se um dado é igual ao outro está vazia e retorna verdadeiro ou falso.
  *
  * @param dado1 Dado a ser verificado.
  * @param dado2 Dado a ser verificado.
  */
  bool igual(T dado1, T dado2) {
    return dado1 == dado2;
  }

  /*!
  * Verifica se um dado é maior que outro está vazia e retorna verdadeiro ou falso.
  *
  * @param dado1 Dado a ser verificado.
  * @param dado2 Dado a ser verificado.
  */
  bool maior(T dado1, T dado2) {
    return dado1 > dado2;
  }

  /*!
  * Verifica se um dado é menor que outro está vazia e retorna verdadeiro ou falso.
  *
  * @param dado1 Dado a ser verificado.
  * @param dado2 Dado a ser verificado.
  */
  bool menor(T dado1, T dado2) {
    return dado1 < dado2;
  }

  /*!
  * Destrói a lista.
  */
  void destroiListaDuplo(){
    ElementoDuplo<T> *elemento = this->head;
    for (int i = 0; i < this->size-1; i++) {
      ElementoDuplo<T> *anterior = elemento;
      elemento = elemento->getSucessor();
      delete anterior;
    }
    this->head = NULL;
    this->size = 0;
  }
};

