// Copyright 2015 Lucas Mauro de Souza

#include "Elemento.hpp"

/*!
* Representa uma lista. Isto é uma estrutura de dados onde cada novo elemento 
* pode ser adicionado em qualquer posição, desde que esta esyeja dentro do tamanho limite da estrutura.
* Armazenando um dado em uma determinada posição da lista fará com que os dados seguintes sejam deslocados 
* na estrutura, um a um, de modo que haja espaço para a alocação deste dado a ser inserido e não se perca 
* o dado que ocupava anteriormente a posição onde o novo dado está sendo inserido.
*/

template<typename T>
class ListaEnc {
 protected:
  Elemento<T>* head;  //! indica o primeiro elemento da lista.
  int size;  //! indica o  tamanho da lista.

 public:
  ListaEnc() {
    this->size = 0;  //! inicializa a lista ao colocar seu tamanho igual a zero.
  }

  ~ListaEnc() {
    this->destroiLista();  //! chama o metodo destrutor da lista.
  }

  /*!
  *  Adiciona um elemento ao início da lista.
  */
  void adicionaNoInicio(const T& dado) {
    Elemento<T> *primeiro = this->head;
    this->head = new Elemento<T>(dado, primeiro);
    this->size++;
  }

  /*!
  *  Retira um elemento ao início da lista.
  */
  T retiraDoInicio() {
    if (this->listaVazia()) {
      throw("A lista está vazia; impossível retirar elementos.");
    } else {
      Elemento<T> *newHead = this->head;
      T dado = this->head->getInfo();
      this->head = newHead->getProximo();
      this->size--;
      delete newHead;
      return dado;
    }
  }

  /*!
  *  Elimina (deleta) um elemento ao início da lista.
  */
  void eliminaDoInicio() {
      if (this->listaVazia()) {
        throw("A lista está vazia; impossível retirar elementos.");
    } else {
      Elemento<T> *inicio = this->head;
      Elemento<T> *atual = inicio->getProximo();
      this->head = atual;
      delete inicio;
      this->size--;
    }
  }

  /*!
  *  Adiciona um elemento à posição especificada na lista.
  */
  void adicionaNaPosicao(const T& dado, int pos) {
    if (pos == 0) {
      return this->adicionaNoInicio(dado);
    } else if (pos < 0 || pos > this->size) {
      throw "Posição inválida.";
    }

    Elemento<T> *elemento = this->head;
    Elemento<T> *proximo;
    for (int i = 1; i < pos; i++) {
      elemento = elemento->getProximo();
    }
    proximo = elemento->getProximo();
    elemento->setProximo(new Elemento<T>(dado, proximo));
    this->size++;
  }

  /*!
  *  Retorna a posição em que tal dado se encontra na lista,
  *  ou lança uma exceção caso não se encontre este dado.
  */
  int posicao(const T& dado) const {
    Elemento<T> *elemento = this->head;
    for (int i = 0; i < this->size; i++) {
      if (elemento->getInfo() == dado) {
        return i;
      }
      elemento = elemento->getProximo();
    }
    throw "Elemento não encontrado.";
  }

  /*!
  *  Retorna a posição de memória em que tal dado se encontra na lista,
  *  ou lança uma exceção caso não se encontre este dado.
  */
  T* posicaoMem(const T& dado) const {
    Elemento<T> *elemento = this->head;
    for (int i = 0; i < this->size; i++) {
      if (elemento->getInfo() == dado) {
        T valor = elemento->getInfo();
        return &valor;
      }
      elemento = elemento->getProximo();
    }
    throw "Elemento não encontrado.";
  }

  /*!
  *  Retorna verdadeiro ou falso se algum dado está contido na lista.
  */
  bool contem(const T& dado) {
    Elemento<T> *elemento = this->head;
    for (int i = 1; i <= this->size; i++) {
      if (elemento->getInfo() == dado) {
        return true;
      }
      elemento = elemento->getProximo();
    }
    return false;
  }

  /*!
  *  Retira um elemento da lista a partir de sua posição.
  */
  T retiraDaPosicao(int pos) {
    if (pos == 0) {
        return this->retiraDoInicio();
    } else if (pos < 0 || pos > this->size) {
        throw "Posição inválida";
    }
    Elemento<T> *elemento = this->head;
    for (int i = 1; i < pos; i++) {
      elemento = elemento->getProximo();
    }
    Elemento<T> *proximo = elemento->getProximo();
    T valor = proximo->getInfo();
    elemento->setProximo(proximo->getProximo());
    delete proximo;
    this->size--;
    return valor;
  }

  /*!
  *  Adiciona um elemento ao fim da lista.
  */
  void adiciona(const T& dado) {
    if (this->listaVazia()) {
        return this->adicionaNoInicio(dado);
    }
    Elemento<T> *ultimo;
    Elemento<T> *posicao = this->head;
    for (int i = 1; i < this->size; i++) {
      if (posicao->getProximo() != NULL) {
        posicao = posicao->getProximo();
      }
    }
    ultimo = new Elemento<T>(dado, NULL);
    posicao->setProximo(ultimo);
    this->size++;
  }

  /*!
  *  Retira um elemento do fim da lista.
  */
  T retira() {
    if (this->listaVazia()) {
      throw("A lista está vazia; impossível retirar elementos.");
    }
    T dado;
    Elemento<T> *elemento = this->head;
    for (int i = 1; i <= this->size; i++) {
      if (elemento->getProximo() == NULL) {
        dado = elemento->getInfo();
      }
      elemento = elemento->getProximo();
    }
    delete elemento;
    return dado;
  }

  /*!
  *  Retira um elemento elemento específico da lista.
  */
  T retiraEspecifico(const T& dado) {
    int posicao = this->posicao(dado);
    return this->retiraDaPosicao(posicao);
  }

  /*!
  * Adiciona um elemento à lista em ordem de tamanho.
  */
  void adicionaEmOrdem(const T& data) {
     if (this->listaVazia()) {
      return this->adicionaNoInicio(data);
    }
    Elemento<T> *elemento = this->head;
    int posicao = 1;
    while (posicao < this->size && this->maior(data, elemento->getInfo())) {
      elemento = elemento->getProximo();
      posicao++;
    }
    if (this->maior(data, elemento->getInfo())) {
      this->adicionaNaPosicao(data, posicao+1);
    } else {
      this->adicionaNaPosicao(data, posicao);
    }
  }

  /*!
  * Verifica se a lista está vazia.
  */
  bool listaVazia() const {
    return this->size == 0;
  }

  /*!
  * Verifica se dado1 é igual a dado2.
  */
  bool igual(T dado1, T dado2) {
    return dado1 == dado2;
  }

  /*!
  * Verifica se dado1 é maior que dado2.
  */
  bool maior(T dado1, T dado2) {
    return dado1 > dado2;
  }

  /*!
  * Verifica se dado1 é menor que dado2.
  */
  bool menor(T dado1, T dado2) {
    return dado1 < dado2;
  }

  /*!
  * Retorna o tamanho da lista.
  */
  int tamanho() {
    return this->size;
  }

  /*!
  * Destrói a lista e seus respectivos elementos.
  */
  void destroiLista() {
    Elemento<T> *atual = this->head;
    for (int i = 1; i <= this->size; i++) {
        Elemento<T> *anterior = atual;
        atual = atual->getProximo();
        delete anterior;
    }
    this->head = NULL;
    this->size = 0;
  }
};

