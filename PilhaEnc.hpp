// Copyright 2015 Lucas Mauro de Souza

#include "ListaEnc.hpp"

/*!
* Representa uma Pilha encadeada. Isto é uma estrutura de dados onde cada novo elemento 
* é adicionado ao topo, e pode apenas ser retirado pelo topo.
*/

template<typename T>
class PilhaEnc : private ListaEnc<T> {
 public:
    PilhaEnc() {
    }

    ~PilhaEnc() {
        ListaEnc<T>::destroiLista();
    }

    /*!
    * Insere um dado do tipo T (genérico) enviado pelo usuário ao topo da pilha.
    *
    * @param dado dado enviado pelo usuário, para ser inserido à pilha.
    */
    void empilha(const T& dado) {
        ListaEnc<T>::adicionaNoInicio(dado);
    }

    /*!
    * Remove e retorna o elemento do topo da pilha.
    */
    T desempilha() {
        return ListaEnc<T>::retiraDoInicio();
    }

    /*!
    * Retorna o elemento do topo da pilha, sem o remover da estrutura.
    */
    T topo() {
        T elemento = ListaEnc<T>::retiraDoInicio();
        ListaEnc<T>::adicionaNoInicio(elemento);
        return elemento;
    }

    /*!
    * Esvazia a pilha.
    */
    void limparPilha() {
        ListaEnc<T>::destroiLista();
    }

    /*!
    * Verifica se a pilha está vazia e retorna verdadeiro ou falso.
    */
    bool PilhaVazia() {
        return ListaEnc<T>::size == 0;
    }
};
