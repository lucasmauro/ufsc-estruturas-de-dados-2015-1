// Copyright 2015 Lucas Mauro de Souza

/*!
* Representa uma lista. Isto é uma estrutura de dados onde cada novo elemento 
* pode ser adicionado em qualquer posição, desde que esta esyeja dentro do tamanho limite da estrutura.
* Armazenando um dado em uma determinada posição da lista fará com que os dados seguintes sejam deslocados 
* na estrutura, um a um, de modo que haja espaço para a alocação deste dado a ser inserido e não se perca 
* o dado que ocupava anteriormente a posição onde o novo dado está sendo inserido.
*/
template <typename T>
class Lista {
 private:
    T * vetor;  //! memória para alocação de dados.
    int tamanhoMaximo;  // Tamanho máximo que a lista poderá ter.
    int tamanhoAtual;  // Tamanho atual da lista.

    /*!
    * Verifica se a lista está cheia, e consequentemente a possibilidade de se adicionar 
    * um dado à lista numa posição indeterminada. Em caso negativo, lança uma exeção. 
    * Em caso positivo, retorna um valor boolean true.
    */
    bool possivelAdicionar() {
        if (this->listaCheia()) {
            throw("A lista está cheia; impossível adicionar elementos.");
        } else {
            return true;
        }
    }

    /*!
    * Verifica se a lista está cheia e a possibilidade de armazenar um dado num destino específico na lista.
    * Em caso negativo, lança uma exeção. Em caso positivo, retorna um valor boolean true.
    */
    bool possivelAdicionar(int destino) {
        if (this->possivelAdicionar()) {
            if (destino >= this->tamanhoMaximo || destino < 0) {
                throw("Posição inválida para adição de elementos");
            }
        } else {
            return true;
        }
    }

    /*!
    * Verifica se a lista está vazia, e consequentemente a possibilidade de se retirar um dado da lista.
    * Em caso negativo, lança uma exeção. Em caso positivo, retorna um valor boolean true.
    */
    bool possivelRemover() {
        if (this->listaVazia()) {
            throw("A lista está vazia; impossível retirar elementos.");
        } else {
            return true;
        }
    }

    /*!
    * Verifica se a lista está vazia e a possibilidade de remover um dado numa posição específica na lista.
    * Em caso negativo, lança uma exeção. Em caso positivo, retorna um valor boolean true.
    */
    bool possivelRemover(int posicao) {
        if (this->possivelRemover()) {
            if (posicao > this->tamanhoAtual || posicao < 0) {
                throw("Posição inválida para retirar elementos");
            }
        } else {
            return true;
        }
    }

 public:
    Lista() {
        //! Inicializa um espaço para armazenamento de dados.
        this->vetor = new T[500];
        //! Inicializa a lista.
        this->tamanhoAtual = -1;
    }

    explicit Lista(int tam) {
        if (tam < 1) {
            throw("Impossível criar lista com número negativo de elementos.");
        } else {
            /*!
            * Declara o tamanho máximo que a lista pode 
            * ter como valor especificado pelo usuário.
            */
            this->tamanhoMaximo = tam;
            /*!
            * Inicializa um espaço com tamanho especificado pelo usuário 
            * para armazenamento de dados.
            */
            this->vetor = new T[tam];
            //! Inicializa a lista.
            this->tamanhoAtual = -1;
        }
    }


   /*!
   * Adiciona um dado ao fim da lista, se houver espaço.
   */
    void adiciona(T dado) {
        if (this->possivelAdicionar()) {
            this->tamanhoAtual += 1;
            this->vetor[this->tamanhoAtual] = dado;
        }
    }

    /*!
    * Adiciona um dado ao início da lista, se houver espaço.
    */
    void adicionaNoInicio(T dado) {
        if (this->possivelAdicionar()) {
           this->adicionaNaPosicao(dado, 0);
        }
    }

    /*!
    * Adiciona um dado ao início da lista, se houver espaço e o destino for válido.
    */
    void adicionaNaPosicao(T dado, int destino) {
        if (this->possivelAdicionar(destino)) {
            this->tamanhoAtual += 1;
            for (int i = this->tamanhoAtual; i > destino; i--) {
                this->vetor[i] = this->vetor[i-1];
            }

            this->vetor[destino] = dado;
        }
    }

    /*!
    * Adiciona um dado à lista após outro dado do qual ele seja maior, 
    * e se houver um dado maior após, antes deste maior.
    */
    void adicionaEmOrdem(T dado) {
        int posicao = 0;
        if (this->possivelAdicionar()) {
            while (posicao <= this->tamanhoAtual
                && dado > this->vetor[posicao]) {
                posicao +=1;
            }

            this->adicionaNaPosicao(dado, posicao);
        }
    }

    /*!
    * Retira um dado do fim da lista, se ela não estiver vazia.
    */
    T retira() {
        if (this->possivelRemover()) {
            this->tamanhoAtual -= 1;
            return this->vetor[this->tamanhoAtual+1];
        }
    }

    /*!
    * Retira um dado do início da lista, se ela não estiver vazia.
    */
    T retiraDoInicio() {
        if (this->possivelRemover()) {
            return this->retiraDaPosicao(0);
        }
    }

    /*!
    * Retira um dado de uma posição específica da lista se ela não estiver vazia
    * e a posição for válida.
    */
    T retiraDaPosicao(int posicao) {
        if (this->possivelRemover(posicao)) {
            T dado = this->vetor[posicao];
            for (int i = posicao; i <= this->tamanhoAtual; i++) {
                this->vetor[i] = this->vetor[i+1];
            }

            this->tamanhoAtual -= 1;

            return dado;
        }
    }

    /*!
    * Retira um dado de específico da lista se ela não estiver vazia
    * e o dado existir.
    */
    T retiraEspecifico(T dado) {
        if (this->possivelRemover()) {
            int posicao;
            for (int i = 0; i < this->tamanhoAtual; i++) {
                if (this->vetor[i] == dado) {
                    posicao = i;
                }
            }
            this->retiraDaPosicao(posicao);
        }
    }

    /*!
    * Retorna a posição onde um determinado dado está alocado.
    */
    int posicao(T dado) {
        for (int i = 0; i <= this->tamanhoAtual; i++) {
            if (this->vetor[i] == dado) {
                return i;
            }
        }
        throw("Dado inexistente.");
    }

    /*!
    * Retorna a confirmação ou negação de que há um determinado dado na lista.
    */
    bool contem(T dado) {
        for (int i = 0; i <= this->tamanhoAtual; i++) {
            if (this->vetor[i] == dado) {
                return true;
            }
        }
        return false;
    }

    /*!
    * Verifica se dois dados são iguais e retorna verdadeiro ou falso.
    */
    bool igual(T dado1, T dado2) {
        return dado1 == dado2;
    }

    /*!
    * Verifica um dado é maior que outro e retorna verdadeiro ou falso.
    */
    bool maior(T dado1, T dado2) {
        return dado1 > dado2;
    }

    /*!
    * Verifica um dado é menor que outro e retorna verdadeiro ou falso.
    */
    bool menor(T dado1, T dado2) {
        return dado1 < dado2;
    }

    /*!
    * Verifica se a lista está cheia e retorna verdadeiro ou falso.
    */
    bool listaCheia() {
        return (this->tamanhoAtual + 1) >= this->tamanhoMaximo;
    }

    /*!
    * Verifica se a lista está cheia e retorna verdadeiro ou falso.
    */
    bool listaVazia() {
        return this->tamanhoAtual == -1;
    }

    /*!
    * Destrói a lista. Define o tamanho atual dela para -1.
    */
    void destroiLista() {
        this->tamanhoAtual = -1;
    }
};